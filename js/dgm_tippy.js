(function ($, Drupal, drupalSettings, once) {
  Drupal.behaviors.dgmTippy =  {
    attach: function (context, settings) {
      let $elements = $(once('dgmTippy', '[data-tippy-target], [data-tippy-content]', context));
      $elements.each( function() {
        let $this = $(this);
        if ($this.attr('data-tippy-target')) {
          tippy(
            this, {
              content: document.querySelector($this.attr('data-tippy-target')),
              allowHTML: true
            });
        }
        else if ($this.attr('data-tippy-content')) {
          tippy(
            this, {
              content: $this.attr('data-tippy-content'),
            });
        }
      });
    }
  }
} (jQuery, Drupal, drupalSettings, once));
