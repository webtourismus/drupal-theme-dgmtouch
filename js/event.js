(function ($, Drupal, drupalSettings, once) {
  Drupal.behaviors.dgm_event_flatpickr = {
    attach: function (context, settings) {
      let inputElement = once('dgm_event_flatpickr', 'input[name="field_dates_value"]').shift();
      let defaultDate = (new URLSearchParams(document.location.search)).get('field_dates_value')
      flatpickr(inputElement, {
        altInput: true,
        altFormat: "d.m.Y",
        dateFormat: "Y-m-d",
        defaultDate: defaultDate || "today",
        minDate: "today",
        locale: drupalSettings?.path?.currentLanguage || 'de',
        disableMobile: true,
      });
    }
  }
}(jQuery, Drupal, drupalSettings, once));

