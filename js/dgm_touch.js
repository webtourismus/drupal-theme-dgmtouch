(function ($, Drupal, drupalSettings, once) {

  Drupal.behaviors.wt_dgm_refresh_startpage = Drupal.behaviors.wt_dgm_refresh_startpage || {
    attach: function (context, settings) {
      if (!window.location.hostname.includes('-iframe.')) {
        window.setTimeout(function() {
          window.location.href = '/';
        }, 15 * 60 * 1000);
      }
    }
  };


  Drupal.behaviors.wt_dgm_backlink = Drupal.behaviors.wt_dgm_backlink || {
    attach: function (context, settings) {
      let $elements = $(once('wt.dgm.backlink', '.js-backlink, [href*="#js-backlink"]', context));
      $elements.each(function () {
        $(this).attr('href', document.referrer);
        $(this).on('click', function() {
          history.back();
          return false;
        });
      });
    }
  };

  Drupal.behaviors.wt_dgm_langbtn = Drupal.behaviors.wt_dgm_langbtn || {
    attach: function (context, settings) {
      let $elements = $(once('wt.dgm.langbtn', '.language-switcher__btn', context));
      $elements.on('click', function() {
        $(this).siblings('.language-switcher__ul').slideToggle();
      });
    }
  };

  Drupal.behaviors.wt_dgm_override_pagetitle = Drupal.behaviors.wt_dgm_override_pagetitle || {
    attach: function (context, settings) {
      let $elements = $(once('wt_dgm_override_pagetitle', '.js-override-title', context));
      $elements.each(function () {
        /** views contextual filter title and translations don't work with drupal title block */
        let title = $(this).html();
        $('.pagehead h1').html( title );
      });
    }
  };

  Drupal.behaviors.wt_dgm_relocate_views_switcher = Drupal.behaviors.wt_dgm_relocate_views_switcher || {
    attach: function (context, settings) {
      let $elements = $(once('wt.dgm.relocate_views_switcher', '.views-id--dgm, .views-id--event', context));
      $elements.each( function() {
        if ($(this).find('footer').length != 1) {
          return;
        }

        let $container = $('<div class="views__toggle"></div>');
        let $parent = $(this).find('.views-attachment-before, .views-exposed-form[data-drupal-selector^=views-exposed-form-event]').first();
        $parent.addClass('has-views-switcher').append($container);
        let $tiles = $('<i class="views__toggleicon views__toggleicon--tiles fal fa-border-all"></i>');
        $container.append($tiles);
        let $globe = $('<i class="views__toggleicon views__toggleicon--globe fal fa-globe"></i>');
        $container.append($globe);
        let $link = $(this).find('footer > a');
        if ($link.length < 1) {
          $link = $('<button disabled class="views__togglebtn views__togglebtn--clickable button"></button>');
        }
        else {
          $link.addClass('views__togglebtn views__togglebtn--clickable button').empty();
        }
        let $unclickableButton = $('<button disabled class="views__togglebtn views__togglebtn--unclickable button"></button>');
        if ($(this).attr('class').includes('_map')) {
          $tiles.wrap($link);
          $globe.wrap($unclickableButton);
        }
        else {
          $globe.wrap($link);
          $tiles.wrap($unclickableButton);
        }
        $tiles.after(Drupal.t('List'));
        $globe.after(Drupal.t('Map'));

        $(this).find('footer').remove();
      });
    }
  };

  Drupal.behaviors.wt_dgm_relocate_views_exposed_form = Drupal.behaviors.wt_dgm_relocate_views_exposed_form || {
    attach: function (context, settings) {
      let $elements = $(once('wt.dgm.wt_dgm_relocate_views_exposed_form', '.views-id--dgm', context));
      $elements.each( function() {
        if ($(this).find('.views-exposed-form').length != 1) {
          return;
        }

        let $form = $(this).find('.views-exposed-form').first();
        let $views_toogle = $(this).find('.views__toggle');
        let $attachement = $(this).find('.views-attachment-before');
        if ($views_toogle.length >= 1) {
          $form.insertBefore($views_toogle.first());
        }
        else {
          $attachement.append($form);
        }
        $attachement.addClass('has-textsearch')
      });
    }
  };

  Drupal.behaviors.wt_dgm_remove_empty_facets = Drupal.behaviors.wt_dgm_remove_empty_facets || {
    attach: function (context, settings) {
      let $elements = $(once('wt.dgm.wt_dgm_remove_empty_facets', '.views-id--dgm', context));
      $elements.each( function() {
        if ($(this).find('.facet-empty.facet-hidden').length != 1) {
          return;
        }
        $(this).find('.facet-empty.facet-hidden').remove();
      });
    }
  };

  Drupal.behaviors.wt_dgm_move_form_actions = Drupal.behaviors.wt_dgm_move_form_actions || {
    attach: function (context, settings) {
      let $elements = $(once('wt.dgm.wt_dgm_move_form_actions', '.views-id--event', context));
      $elements.each( function() {
        $(this).find('.views-exposed-form').prepend($(this).find('.views-exposed-form .form-actions'));
      });
    }
  };

  Drupal.behaviors.wt_dgm_views_form_autosubmit = Drupal.behaviors.wt_dgm_views_form_autosubmit || {
    attach: function (context, settings) {
      let $elements = $(once('wt.dgm.wt_dgm_views_form_autosubmit', '.views-exposed-form', context));
      $elements.each( function() {
        $(this).find('.js-form-item select, input[type=text]').on('change', function() {
          $(this).closest('form').submit();
        });
      });
    }
  };

  Drupal.behaviors.wt_dgm_infopoint_menu_toggle = Drupal.behaviors.wt_dgm_infopoint_menu_toggle || {
    attach: function (context, settings) {
      let $elements = $(once('wt_dgm_infopoint_menu_toggle', '.pagefoot__menubtn', context));
      $elements.on('click', function() {
        $(this).attr('aria-expanded', $(this).attr('aria-expanded') == 'true' ? 'false' : 'true');
        $('.pagefoot__below').slideToggle();
      });
    }
  };

  Drupal.behaviors.wt_dgm_infopoint_menu_scroll = Drupal.behaviors.wt_dgm_infopoint_menu_scroll || {
    attach: function (context, settings) {
      let $elements = $(once('wt_dgm_infopoint_menu_scroll', '.infopointmenu__btn', context));
      $elements.on('click', function() {
        if ($('.tile-container--menu').hasClass('is-unstable')) {
          return;
        }
        $('.tile-container--menu').addClass('is-unstable')
        let gap = getComputedStyle(document.querySelector('.pagefoot')).getPropertyValue('--tile--gap');
        if (gap) {
          gap = parseInt(gap.replace(/px/, '').trim());
        }
        else {
          gap = 0;
        }
        let scrollDelta = ($('.tile-container--menu > :first-child').length > 0) ? ($('.tile-container--menu > :first-child').width() + gap) : 0;
        let operator = '';
        if ($(this).hasClass('infopointmenu__btn--prev')) {
          operator = '-=';
          scrollDelta = Math.min(scrollDelta, $('.tile-container--menu').scrollLeft());
        }
        else if ($(this).hasClass('infopointmenu__btn--next')) {
          operator = '+=';
          let elementPosLeft = $('.tile-container--menu > :last-child').position().left;
          let containerWidth = $('.tile-container--menu').width();
          let elementWidth = $('.tile-container--menu > :last-child').width();
          let elementRightPoint = elementPosLeft + elementWidth;
          if (elementRightPoint <= containerWidth) {
            scrollDelta = 0;
          }
          let overflow = elementRightPoint - containerWidth;
          scrollDelta = Math.min(scrollDelta, overflow);
        }
        $('.tile-container--menu').animate(
          {'scrollLeft': operator + scrollDelta  + 'px'},
          {'complete': function() {
              $('.tile-container--menu').removeClass('is-unstable');
            }
          }
        );
      });
    }
  };

  Drupal.behaviors.wt_dgm_visibility_toggle = Drupal.behaviors.wt_dgm_visibility_toggle || {
    attach: function (context, settings) {
      let $elements = $(once('wt_dgm_visibility_toggle', '[data-toggle]', context));
      $elements.on('click', function (ev) {
        $source = $(this);
        $target = $($source.attr('data-target'));
        toggleMode = $source.attr('data-toggle');
        animationProperties = {};
        animationOptions = {};

        if (toggleMode == 'collapse') {
          functionNameToggleToExpand = 'slideDown';
          functionNameToggleToCollapse = 'slideUp';
        }
        else if (toggleMode == 'fade') {
          functionNameToggleToExpand = 'fadeIn';
          functionNameToggleToCollapse = 'fadeOut';
        }
        else if (toggleMode == 'flyout') {
          functionNameToggleToExpand = 'animate';
          functionNameToggleToCollapse = 'animate';
          animationProperties = {'width': 'toggle'};
        }
        else {
          console.error('Unknown data-toggle value. Use collapse (animate height), fade (animate opacity) or flyout (animate width).');
          return;
        }

        if ($source.attr('data-animation-duration') == parseInt($source.attr('data-animation-duration'), 10)) {
          animationOptions.duration = $source.attr('data-animation-duration');
        }
        else {
          animationOptions.duration = 400;
        }

        if (!$target.hasClass('js-unstable')) {
          if ($target.hasClass('js-expanded')) {
            animationOptions.complete = function() {
              $source.attr('aria-expanded', 'false');
              $target.removeClass('js-expanded').removeClass('js-unstable').addClass('js-collapsed');
            }

            $target.addClass('js-unstable');
            if (!$.isEmptyObject(animationProperties)) {
              $target[functionNameToggleToCollapse](animationProperties, animationOptions);
            }
            else {
              $target[functionNameToggleToCollapse](animationOptions);
            }
          }
          else {
            animationOptions.complete = function() {
              $source.attr('aria-expanded', 'true');
              $target.removeClass('js-collapsed').removeClass('js-unstable').addClass('js-expanded');
            }

            $target.addClass('js-unstable');
            if ($source.attr('data-toggle-to-display')) {
              // toggle to that specific display-property when fully visible
              // needed when initially toggling from "display: none" or
              // to a state other then the elements initial display property
              $target.css('display', $source.attr('data-toggle-to-display')).hide();
              // once done this is stored in jQuery's internal data store and no longer needed
              $source.removeAttr('data-toggle-to-display');
            }
            $target.addClass('js-unstable');
            if (!$.isEmptyObject(animationProperties)) {
              $target[functionNameToggleToExpand](animationProperties, animationOptions);
            }
            else {
              $target[functionNameToggleToExpand](animationOptions);
            }
          }
        }
        ev.preventDefault();
      });

    }
  };

  Drupal.behaviors.wt_dgm_az = Drupal.behaviors.wt_dgm_az || {
    attach: function (context, settings) {
      let $elements = $(once('wt_dgm_az', '.node--a_z--full', context));
      $elements.each(function () {
        const observer = new IntersectionObserver(entries => {
          entries.forEach(entry => {
            let id = entry.target.getAttribute('id');
            if (entry.intersectionRatio > 0) {
              document.querySelector(`.a_z__letter a[href="#${id}"]`).classList.add('a_z__link--active');
            } else {
              document.querySelector(`.a_z__letter  a[href="#${id}"]`).classList.remove('a_z__link--active');
            }
          });
        });

        document.querySelectorAll('.a_z__section').forEach((section) => {
          observer.observe(section);
        });
      });
    }
  }

}(jQuery, Drupal, drupalSettings, once));
