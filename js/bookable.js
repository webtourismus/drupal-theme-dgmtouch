(function ($, Drupal, drupalSettings, once) {
  Drupal.behaviors.bookable_date_switch = {
    attach: function (context, settings) {
      let elements = once('bookable_date_switch', 'select.bookable__dateselect');
      $(elements).each( function() {
        $(this).on('change', function() {
          $(this).parents('.bookable--ressource').find('.bookable__date').slideUp();
          $(this).parents('.bookable--ressource').find(`.bookable__date[data-bookable-date-switch-target=${$(this).val()}]`).slideDown();
        });
        let searchParam = new URLSearchParams(window.location.search)
        let day = searchParam.get('preselect');
        if (!day) return;
        let idx = Array.from(this.options).findIndex(option => option.label === day);
        if (idx >= 1) {
          this.selectedIndex = idx;
          $(this).change();
        }
      });
    }
  }
}(jQuery, Drupal, drupalSettings, once));

